package fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class BadgeSizeCellRenderer extends DefaultBadgeCellRenderer {

    private Color originBackgroud;

    private Color originForeGround;

    public BadgeSizeCellRenderer() {
        super();
        this.originBackgroud = this.getBackground();
        this.originForeGround = this.getForeground();
    }

    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

      Long size = (Long) value;
        setText(humanReadableByteCountBin(size));
        if (!isSelected) {
            setForeground(this.originBackgroud);
            if (size > 10000) {
                setBackground(Color.ORANGE);
            } else {
                setBackground(this.originBackgroud);
            }
        }
        else{
            if (size > 10000) {
                setForeground(Color.RED);
            } else {
                setForeground(this.originBackgroud);
            }
        }

        return this;
    }

}
