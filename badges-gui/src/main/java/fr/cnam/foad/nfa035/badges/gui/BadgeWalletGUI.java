package fr.cnam.foad.nfa035.badges.gui;


import fr.cnam.foad.nfa035.badges.App;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class BadgeWalletGUI {
    private JButton button1;
    private JPanel panel1;
    private JTable table1;

    private static final String RESOURCES_PATH = "badges-gui/src/main/java/fr/cnam/foad/nfa035/badges/gui/";

    public BadgeWalletGUI() {
        button1.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e the event to be processed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });

        DirectAccessBadgeWalletDAO dao = null;

        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            List<DigitalBadge> tableList = new ArrayList<>();
            tableList.addAll(metaSet);

            BadgeModel badgeModel = new BadgeModel(tableList);
            table1.setModel(badgeModel);

            table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
            table1.setDefaultRenderer(String.class, new DefaultBadgeCellRenderer());
            table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());
            table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

    }

    public static void main(String[] args) {
        JFrame frame =  new JFrame("My Badge Wallet");
        frame.setContentPane(new BadgeWalletGUI().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
