package fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class DefaultBadgeCellRenderer extends javax.swing.table.DefaultTableCellRenderer {

    private Color originForeground = null;

    public DefaultBadgeCellRenderer() {
        this.originForeground = originForeground;
    }

    /**
     *
     * @param table  the <code>JTable</code>
     * @param value  the value to assign to the cell at
     *                  <code>[row, column]</code>
     * @param isSelected true if cell is selected
     * @param hasFocus true if cell has focus
     * @param row  the row of the cell to render
     * @param column the column of the cell to render
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

        if (((Date)table.getModel().getValueAt(row, 3)).before(new Date())){
            setForeground(Color.RED);
        }
        else{
            setForeground(this.originForeground);
        }

        return comp;
    }

}
