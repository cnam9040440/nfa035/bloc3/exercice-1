package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

public class BadgeModel extends AbstractTableModel {



    private List<DigitalBadge> digitalBadgeList;
    private String[] entetes = {"ID", "Code Série", "Début", "Fin", "Taille (octets)"};



    private long serialVersionUID;

    public BadgeModel(List<DigitalBadge> digitalBadgeList) {
        this.digitalBadgeList = digitalBadgeList;
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return digitalBadgeList.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return entetes.length;
    }

    /**
     * {@inheritDoc}
     * @param columnIndex  the column being queried
     * @return
     */
    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                // ID
                return getBadges().get(rowIndex).getMetadata().getBadgeId();

            case 1:
                // Code Serie
                return getBadges().get(rowIndex).getSerial();

            case 2:
                // Debut
                return getBadges().get(rowIndex).getBegin();

            case 3:
                // Fin
                return getBadges().get(rowIndex).getEnd();

            case 4:
                // Taille
                return getBadges().get(rowIndex).getMetadata().getImageSize();

            default:
                throw new IllegalArgumentException();
        }

    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;

            case 3:
                return Date.class;

            case 2:
                return Date.class;

            case 4:
                return Long.class;

            default:
                return Object.class;
        }
    }

    /**
     *
     * @return list des digital badges
     */
    public List<DigitalBadge> getBadges() {
        return digitalBadgeList;
    }


}
